

package edu.ucsd.cse110.library.rules;
import java.time.LocalDate;

import edu.ucsd.cse110.library.Member;
import edu.ucsd.cse110.library.Publication;

public class ManageFees {
	public void checkoutPublication(Member name, Publication title, LocalDate date){
		title.checkout(name,date);
	}
	public void returnPublication(Publication title, LocalDate date){
		title.pubReturn(date);
	}
	public Double getFee(Member name){
		return name.getDueFees();
	}
	public Boolean hasFee(Member name){
		return Math.abs(name.getDueFees())> 0.01;

	}
}